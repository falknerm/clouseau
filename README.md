# What/who? is Clouseau 
Clouseau, much like the cartoon character it's named after, investigates.
Clouseau investigates GitLab groups you want 
looked into. Clouseau takes a deep snapshot of your desired groups, forever preserving the group in that moment of time. 
Viewing the snapshot provides insights into contributions of members and general details of the group. 
Take snapshots over time and compare them to show group dynamics in their projects. 

**Clouseau can't tell the difference between good LOC and bad LOC.**

**Clouseau can't tell the difference between useful commits and bad commits.**

**Clouseau can't tell the difference between meaningful MRs and bad MRs.**


However, from Clouseau's visualizations and charts you can easily determine which group members are leading the pack.
Which group members are in the middle, and most importantly, those who are doing nothing. 

Add the factor of time through comparisons and you can see who is picking up their slack, who is slacking, and who is continuing to do nothing.

No longer do you have to comb through `git log` to do `git blame`!


# What can you do with Clouseau
- Take deep snapshots of your GitLab repo, either through UI, or it's Restful API by selecting Group(s) to snapshot
- Navigate through an individual snapshot, view stats, see visualizations
- Compare a given snapshot against ANY NUMBER of other snapshots and show the difference in it's common groups/projects.
- View Groups, Projects, Commits 
- In Snapshot, bind the commits from alternative committer email addresses to actual users, ensuring accuracy of data and no data loss 


How to Run in Development Environment
-- 
- In one terminal, spin up the web server with `python3 wsgi.py`
- In another terminal, run `ngrok http 5000`
- Navigate to your UoGuelph `GitLab Settings` -> `Applications`
- from the `ngrok` instance copy the randomly generated `http` URL
- In the GitLab `Create Application`, Set the `Redirect URI` to `http://random.ngrok.io/authentication_token`
- navigate to  `http://random.ngrok.io/` in your browser
- Done!

