from flask import Flask, session
from flask_sqlalchemy import SQLAlchemy
from loginpass import create_gitlab_backend, register_to
from authlib.flask.client import OAuth, RemoteApp
from flask_sqlalchemy import SQLAlchemy
from flask_marshmallow import Marshmallow

db = SQLAlchemy()
ma = Marshmallow()
GITLAB_HOST = 'gitlab.socs.uoguelph.ca'
OAUTH_APP_NAME = 'gitlab'
gitlab_backend = create_gitlab_backend(OAUTH_APP_NAME, GITLAB_HOST)
gitlab_backend.OAUTH_CONFIG['client_kwargs'] = {'scope': 'api'}
oauth_client = OAuth()

def fetch_token(name):
    token_session_key = '{}-token'.format(name.lower())
    return session.get(token_session_key, {})


def update_token(name, token):
    token_session_key = '{}-token'.format(name.lower())
    session[token_session_key] = token
    return token


def create_app():
    """Construct the core application."""
    app = Flask(__name__, instance_relative_config=False)
    app.config.from_pyfile("../env.cfg")
    app.app_context().push()
    oauth = OAuth(app, fetch_token=fetch_token, update_token=update_token)
    global oauth_client
    oauth_client = register_to(gitlab_backend, oauth, RemoteApp)
    with app.app_context():
        db.init_app(app)
        from . import routes  # Import routes
        db.create_all()  # Create sql tables for our data models
        ma.init_app(app)
        return app