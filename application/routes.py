from __future__ import division
import random
from flask import render_template, redirect, url_for, jsonify, request
from flask import current_app as app
from .support.GitlabAPI import GitLabAPI
from .support.Snapshotter import Snapshotter
from .models import db, Owner, Snapshot, SnapshotFacts, SnapshotSchema, Group, Project, Commit, Member, \
    MemberGroupRelation, MemberProjectRelation, \
    UserFactTable, UnclaimedEmailCommits, MemberEmailRelation
from . import oauth_client, update_token, OAUTH_APP_NAME, GITLAB_HOST, fetch_token
import datetime
from sqlalchemy import desc

capture_data_threads = {}
import copy
import secrets


@app.route('/api/snapshots', methods=['GET', 'POST'])
def get_api():
    if request.method == 'GET':
        api_key = request.args.get('api_key')
        query = Owner.query.filter_by(api_key=api_key).first()
        if query is None:
            return "BAD API KEY"
        else:
            owner_id = query.id
            snapshot_schema = SnapshotSchema()
            snapshots = (Snapshot.query).filter_by(owner_id=owner_id).order_by(Snapshot.timestamp.desc()).all()
            schema_result = snapshot_schema.dump(snapshots, many=True)
            json = jsonify(schema_result)
            return json
    if request.method == 'POST':

        api_key = request.args.get('api_key')

        query = Owner.query.filter_by(api_key=api_key).first()
        if query is None:
            return "BAD API KEY"
        else:
            group_ids = request.args.get('group_ids')
            name = request.args.get('name')
            list_of_int = group_ids.split(",")
            group_id_list = []
            for i in list_of_int:
                group_id_list.append(int(i))

            thread_id = start_snapshot(query.id, name, group_id_list, query.token)
            return str(thread_id)


@app.template_filter('timeSince')
def timesince(datetime_object):
    converted_d1 = datetime_object
    current_time_utc = datetime.datetime.utcnow()
    return int((current_time_utc - converted_d1).total_seconds())


def get_user_id():
    oauth_client_profile = oauth_client.profile()
    query = Owner.query.filter_by(username=oauth_client_profile.preferred_username).first()
    if query is None:
        return None
    else:
        return query.id


@app.route('/login')
def login():
    redirect_uri = url_for('authenticate_token', _external=True)
    print(redirect_uri)
    return oauth_client.authorize_redirect(redirect_uri)


@app.route('/authenticate_token')
def authenticate_token():
    token = oauth_client.authorize_access_token()
    update_token(OAUTH_APP_NAME, token)
    return redirect(url_for('home', _external=True))


def start_snapshot(user_id, name, groups_selected, gitlab_token):
    global capture_data_threads
    thread_id = random.randint(0, 10000)
    capture_data_threads[thread_id] = Snapshotter(GITLAB_HOST, gitlab_token, user_id, name,
                                                  groups_selected)
    capture_data_threads[thread_id].start()
    return thread_id


@app.route('/profile')
def profile():
    oauth_client_profile = oauth_client.profile()
    owner = Owner.query.filter_by(username=oauth_client_profile.preferred_username).first()
    if get_user_id() is None:
        return render_template("utilities/invalid_token.html")
    else:
        print("User already exists")
        print(owner.id)
        return render_template("profile/profile.html", owner=owner, oauth_client_profile=oauth_client_profile)


@app.route('/progress/<int:thread_id>', methods=['GET'])
def get_snapshot_progress(thread_id):
    global capture_data_threads
    return jsonify(
        {'group_progress': str(capture_data_threads[thread_id].group_progress),
         'project_progress': str(capture_data_threads[thread_id].project_progress),
         'current_group': str(capture_data_threads[thread_id].current_group),
         'current_project': str(capture_data_threads[thread_id].current_project),
         'snapshot_id': str(capture_data_threads[thread_id].snapshot_id)
         }
    )


@app.route('/')
def landing():
    return render_template("landing.html")


@app.route('/commits/<string:snapshot_id>/<int:group_id>/<int:project_id>')
def commits(snapshot_id, group_id, project_id):
    snapshot = (Snapshot.query).filter_by(snapshot_id=snapshot_id).one()
    group = (Group.query).filter_by(id=group_id, snapshot_id=snapshot_id).one()
    project = (Project.query).filter_by(id=project_id, snapshot_id=snapshot_id).one()
    commits = (Commit.query).filter(Commit.created_timestamp <= snapshot.timestamp).filter_by(
        project_id=project_id).order_by(desc(Commit.created_timestamp)).all()
    for commit in commits:
        print(str(commit.project_id) + " " + commit.commit_hash + " " + commit.title)

    token = fetch_token(OAUTH_APP_NAME)
    gitlab_request = GitLabAPI(token["access_token"], GITLAB_HOST)
    if gitlab_request.is_valid_token():
        oauth_client_profile = oauth_client.profile()
        return render_template("commits/commits.html", hostname=GITLAB_HOST,
                               project=project, group=group,
                               commits=commits, snapshot=snapshot, oauth_client_profile=oauth_client_profile)
    else:
        return render_template("utilities/invalid_token.html")


def get_full_fidelity_project(snapshot_id, group_id, project_id):
    owner_id = get_user_id()
    project = (Project.query).filter_by(snapshot_id=snapshot_id, group_id=group_id, id=project_id).one()

    project.members = []
    project_member_ids = (MemberProjectRelation.query).filter_by(project_id=project.id,
                                                                 snapshot_id=snapshot_id).all()
    for project_member_id in project_member_ids:
        project_member = (Member.query).filter_by(id=project_member_id.member_id).one()
        project.members.append(copy.copy(project_member))

    for project_member in project.members:
        project_member_facts = (UserFactTable.query).filter_by(snapshot_id=snapshot_id,
                                                               project_id=project.id,
                                                               member_id=project_member.id,
                                                               ).first()
        if project_member_facts:
            print(project_member_facts)
            project_member.facts = project_member_facts
        else:
            project_member.facts = None
            print("Project member has no facts")

        project_member_emails = (MemberEmailRelation.query).filter_by(member_id=project_member.id,
                                                                      owner_id=owner_id).all()

        if project_member_emails:
            project_member.emails = project_member_emails
        else:
            project_member.emails = None
            print("Project member has no emails")

    unclaimed_emails = (UnclaimedEmailCommits.query).filter_by(snapshot_id=snapshot_id,
                                                               group_id=group_id,
                                                               project_id=project.id).all()
    project.unclaimed_emails = unclaimed_emails
    project.total_unclaimed_commits = 0
    for unclaimed_email in unclaimed_emails:
        project.total_unclaimed_commits += unclaimed_email.commits

    return copy.copy(project)


@app.route('/projects/<string:snapshot_id>/<int:group_id>')
def projects(snapshot_id, group_id):
    # collect required IDs to perform render
    # Get data from database
    group = (Group.query).filter_by(id=group_id, snapshot_id=snapshot_id).one()
    snapshot = (Snapshot.query).filter_by(snapshot_id=snapshot_id).one()
    projects = (Project.query).filter_by(snapshot_id=snapshot_id, group_id=group_id).all()
    # Ensure the token is still valid before showing user information
    token = fetch_token(OAUTH_APP_NAME)
    gitlab_request = GitLabAPI(token["access_token"], GITLAB_HOST)
    owner_id = get_user_id()

    # Solution to jinja2 inability to see an empty python list as empty
    is_projects = False

    for project in projects:
        is_projects = True
        project.members = []
        project_member_ids = (MemberProjectRelation.query).filter_by(project_id=project.id,
                                                                     snapshot_id=snapshot_id).all()
        for project_member_id in project_member_ids:
            project_member = (Member.query).filter_by(id=project_member_id.member_id).one()
            project.members.append(copy.copy(project_member))

    for project in projects:
        for project_member in project.members:
            project_member_facts = (UserFactTable.query).filter_by(snapshot_id=snapshot_id,
                                                                   project_id=project.id,
                                                                   member_id=project_member.id,
                                                                   ).first()
            if project_member_facts:
                print(project_member_facts)
                project_member.facts = project_member_facts
            else:
                project_member.facts = None
                print("Project member has no facts")

            project_member_emails = (MemberEmailRelation.query).filter_by(member_id=project_member.id,
                                                                          owner_id=owner_id).all()

            if project_member_emails:
                project_member.emails = project_member_emails
            else:
                project_member.emails = None
                print("Project member has no emails")

    for project in projects:
        unclaimed_emails = (UnclaimedEmailCommits.query).filter_by(snapshot_id=snapshot_id,
                                                                   group_id=group.id,
                                                                   project_id=project.id).all()
        project.unclaimed_emails = unclaimed_emails
        project.total_unclaimed_commits = 0
        for unclaimed_email in unclaimed_emails:
            project.total_unclaimed_commits += unclaimed_email.commits

    all_members = (Member.query).all()

    if gitlab_request.is_valid_token():
        oauth_client_profile = oauth_client.profile()
        return render_template("projects/projects.html", all_members=all_members, hostname=GITLAB_HOST, group=group,
                               projects=projects, snapshot=snapshot,
                               oauth_client_profile=oauth_client_profile, is_projects=is_projects)
    else:
        return render_template("utilities/invalid_token.html")


@app.route('/groups/<string:snapshot_id>')
def groups(snapshot_id):
    snapshot = (Snapshot.query).filter_by(snapshot_id=snapshot_id).one()
    parent_groups = (Group.query).filter_by(snapshot_id=snapshot_id, parent_group_id=None).all()

    '''
        Link data together in structure that can be visually represented 
        Example: 
        
        parent group: 
            - members
                - member 1
                - member 2
            - subgroups 
                - subgroup 1
                    - members 
                        - member 1
                        - member 2
                - subgroup 2
                    - members 
                        - member 1
                        - member 2
    '''

    for parent_group in parent_groups:
        subgroups = (Group.query).filter_by(snapshot_id=snapshot_id, parent_group_id=parent_group.id).all()
        if subgroups:
            parent_group.subgroups = subgroups

            for subgroup in parent_group.subgroups:
                subgroup.members = []
                subgroup_member_ids = (MemberGroupRelation.query).filter_by(group_id=subgroup.id,
                                                                            snapshot_id=snapshot_id).all()
                for subgroup_member_id in subgroup_member_ids:
                    subgroup_member = (Member.query).filter_by(id=subgroup_member_id.member_id).one()
                    subgroup.members.append(subgroup_member)
        else:
            parent_group.subgroups = None

        parent_group.members = []
        group_member_ids = (MemberGroupRelation.query).filter_by(group_id=parent_group.id,
                                                                 snapshot_id=snapshot_id).all()
        for group_member_id in group_member_ids:
            member = (Member.query).filter_by(id=group_member_id.member_id).one()
            parent_group.members.append(member)

    print(parent_groups)

    token = fetch_token(OAUTH_APP_NAME)
    gitlab_request = GitLabAPI(token["access_token"], GITLAB_HOST)
    if gitlab_request.is_valid_token():
        oauth_client_profile = oauth_client.profile()
        return render_template("groups/groups.html", hostname=GITLAB_HOST, groups=parent_groups, snapshot=snapshot,
                               oauth_client_profile=oauth_client_profile)
    else:
        return render_template("utilities/invalid_token.html")


@app.route('/delete_snapshot/<string:snapshot_id>')
def delete_snapshot(snapshot_id):
    print(snapshot_id)
    (Snapshot.query).filter_by(snapshot_id=snapshot_id).delete()
    (Group.query).filter_by(snapshot_id=snapshot_id).delete()
    (SnapshotFacts.query).filter_by(snapshot_id=snapshot_id).delete()
    (UnclaimedEmailCommits.query).filter_by(snapshot_id=snapshot_id).delete()
    (UserFactTable.query).filter_by(snapshot_id=snapshot_id).delete()
    (Project.query).filter_by(snapshot_id=snapshot_id).delete()
    db.session.commit()
    return redirect(url_for('home', _external=True))


# Claim the commit to the member, then redirect to the claim
@app.route('/claim_commit/<string:claim_id>/<int:member_id>/<int:group_id>/<string:snapshot_id>/<int:project_id>')
def claim_commit(claim_id, member_id, group_id, snapshot_id, project_id):
    owner_id = get_user_id()
    print(" -- CLAIMING EMAIL ---")
    print("Unclaimed Commit ID: " + claim_id)
    print("Claiming Member ID: " + str(member_id))
    print("Snapshot ID: " + snapshot_id)
    print("Group ID: " + str(group_id))
    print("Project ID: " + str(project_id))
    print("Owner ID: " + str(owner_id))
    print(" ----------------------")

    unclaimed_email_commit = (UnclaimedEmailCommits.query).filter_by(id=claim_id).first()
    print(unclaimed_email_commit)
    member_new_email = unclaimed_email_commit.email
    print(member_new_email)

    is_member_in_db = (MemberEmailRelation.query).filter_by(member_id=member_id, email=member_new_email,
                                                                   owner_id=owner_id).first()
    if is_member_in_db:
        print("Member is already a part of this project, no need to add them again!")
    else:
        print("Member is not already in the project")
        member_group_relation = MemberEmailRelation(member_id, member_new_email, owner_id)
        db.session.add(member_group_relation)
        db.session.commit()

    # copy stats from unclaimed commit into existing data around user

    # Get the existing facts of the User, if they exist, so we can add them together

    db.session.delete(unclaimed_email_commit)
    db.session.commit()

    project_member_facts = (UserFactTable.query).filter_by(snapshot_id=snapshot_id,
                                                           project_id=project_id,
                                                           member_id=member_id,
                                                           ).first()
    if project_member_facts:
        project_member_facts.commits = unclaimed_email_commit.commits + project_member_facts.commits
        project_member_facts.total_lines_changed = unclaimed_email_commit.total_lines_changed + project_member_facts.total_lines_changed
        project_member_facts.lines_removed = unclaimed_email_commit.lines_removed + project_member_facts.lines_removed
        project_member_facts.lines_added = unclaimed_email_commit.lines_added + project_member_facts.lines_added
        db.session.commit()
    else:
        user_fact = UserFactTable(snapshot_id=snapshot_id, project_id=project_id,
                                  member_id=member_id,
                                  commits=unclaimed_email_commit.commits,
                                  lines_added=unclaimed_email_commit.lines_added,
                                  lines_removed=unclaimed_email_commit.lines_removed,
                                  total_lines_changed=unclaimed_email_commit.total_lines_changed,
                                  )
        db.session.add(user_fact)
        db.session.commit()

    return redirect(url_for('projects', snapshot_id=snapshot_id, group_id=group_id))


@app.route('/api/docs')
def get_docs():
    print('sending docs')
    return render_template('profile/swagger_ui.html')


@app.route('/compare_projects', methods=['POST', 'GET'])
def compare_projects():
    # Id of the group we are looking into
    comparison_project_id = request.form['comparison_project_id']
    snapshots_in_comparison = request.form.getlist('snapshots_selected')
    comparison_group_id = request.form['comparison_group_id']

    # Get the snapshots, ordered by time
    selected_snapshots = (Snapshot.query).filter(Snapshot.snapshot_id.in_(snapshots_in_comparison)) \
        .order_by(Snapshot.timestamp.desc()).all()

    # Get the full fidelity project from each snapshot you want to compare
    project_from_each_snapshot = []
    for snapshot in selected_snapshots:
        project_from_each_snapshot.append(
            copy.copy(get_full_fidelity_project(snapshot.snapshot_id, comparison_group_id, comparison_project_id)))

    oauth_client_profile = oauth_client.profile()

    return render_template("projects/compare_projects.html",
                           oauth_client_profile=oauth_client_profile, snapshots=selected_snapshots,
                           projects=project_from_each_snapshot)


@app.route('/compare_groups', methods=['POST', "GET"])
def compare_groups():
    # Id of the group we are looking into
    comparison_group_id = request.form['comparison_group_id']
    # List of snapshots involved in the comparison  ['asodjkasodjk','asdasdasd','fdgasdasghg']
    snapshots_in_comparison = request.form.getlist('snapshots_selected')
    comparison_group = None
    unique_projects = []
    # Get all the projects, and gather every unique project
    selected_snapshots = (Snapshot.query).filter(Snapshot.snapshot_id.in_(snapshots_in_comparison)).all()
    for snapshot in selected_snapshots:
        snapshot.comparison_group = copy.copy(
            (Group.query).filter_by(snapshot_id=snapshot.snapshot_id, id=comparison_group_id).one())
        # A copy of the group that is being looked at, only needed once and could be improved by passing in the snapshot
        if comparison_group is None:
            comparison_group = copy.copy(
                (Group.query).filter_by(snapshot_id=snapshot.snapshot_id, id=comparison_group_id).one())

        snapshot.comparison_group.projects = copy.copy((Project.query).filter_by(snapshot_id=snapshot.snapshot_id,
                                                                                 group_id=comparison_group_id).all())
        # Generate a list of all the unique projects between the all snapshots
        # There probably is a more pythonic way of doing this.
        # Assuming selected snapshots are chronological, unique projects would also contain the info (automatically via parent snapshotID) of which snapshot the project appeared first in
        for project in snapshot.comparison_group.projects:
            if unique_projects:
                is_current_project_unqiue = True
                for unique_project in unique_projects:
                    if unique_project.id == project.id:
                        is_current_project_unqiue = False
                if is_current_project_unqiue:
                    unique_projects.append(project)
            else:
                unique_projects.append(project)
    oauth_client_profile = oauth_client.profile()

    print(unique_projects)
    return render_template("groups/compare_groups.html",
                           oauth_client_profile=oauth_client_profile, selected_snapshots=selected_snapshots,
                           group=comparison_group, unique_projects=unique_projects)

@app.route('/compare_snapshots', methods=['POST', "GET"])
def compare_snapshots():
    # set local varibales for snapshot IDs
    comparison_snapshot_id = request.form['comparison_snapshot_id']
    snapshots_selected_ids_list = request.form.getlist('snapshots_selected')

    # get comparison snapshot and populate its groups
    comparison_snapshot = (Snapshot.query).filter_by(snapshot_id=comparison_snapshot_id).one()
    comparison_snapshot.groups = copy.copy((Group.query).filter_by(snapshot_id=comparison_snapshot.snapshot_id).all())

    # get the snapshots the user selected to compare against, and populate their groups
    selected_snapshots = (Snapshot.query).filter(Snapshot.snapshot_id.in_(snapshots_selected_ids_list)).all()
    for snapshot in selected_snapshots:
        snapshot.groups = copy.copy((Group.query).filter_by(snapshot_id=snapshot.snapshot_id).all())

    # find the union between the snapshots

    # create a new data structure to show to what they have in common rather than recursive non-sense
    '''
            {
                group.id = [ 0, 1, 0 ,0],
                group.id = [ 1, 1, 1, 1] 
            }
            
            If all members of list are 1, then all snapshots share in common that group
            
    '''

    # Create a datastructure that shows for each group in the comparison group, do other snapshots have it?
    union_matrix = {}
    for comparison_group in comparison_snapshot.groups:
        union_matrix[comparison_group.id] = []
        for other_snapshot in selected_snapshots:
            contains_comparison_group = False
            for group in other_snapshot.groups:
                if group.id == comparison_group.id:
                    contains_comparison_group = True
            union_matrix[comparison_group.id].append(contains_comparison_group)

    groups_in_common_ids = []
    # Get the Ids of all the groups that are shared by all snapshots
    for group_id in union_matrix:
        # all() checks to see if every item in a list is True.
        # All items in this list are true if the all snapshots had a group in common
        if all(union_matrix[group_id]):
            groups_in_common_ids.append(group_id)
            print(" every snapshot contained group " + str(group_id))
        else:
            print("Not every snapshot contained group " + str(group_id))

    print("These snapshots share these groups in common")
    print(groups_in_common_ids)

    # Get the actual groups
    shared_groups = []
    for group in comparison_snapshot.groups:
        if group.id in groups_in_common_ids:
            shared_groups.append(group)

    oauth_client_profile = oauth_client.profile()

    all_snapshots = copy.copy(selected_snapshots)
    all_snapshots.append(comparison_snapshot)

    return render_template("snapshots/compare_snapshots.html",
                           oauth_client_profile=oauth_client_profile, comparison_snapshot=comparison_snapshot,
                           selected_snapshots=selected_snapshots, all_snapshots=all_snapshots,
                           shared_groups=shared_groups)

    # snapshot_1 = (Snapshot.query).filter_by(snapshot_id=parent_snapshot_id).one()
    # snapshot_2 = (Snapshot.query).filter_by(snapshot_id=snapshot_id).one()
    #
    # snapshot_1_groups = (Group.query).filter_by(snapshot_id=parent_snapshot_id).all()
    # snapshot_2_groups = (Group.query).filter_by(snapshot_id=snapshot_id).all()
    #
    # # find union between snapshots
    # shared_groups = []
    # for snap_1_group in snapshot_1_groups:
    #     for snap_2_group in snapshot_2_groups:
    #         print(str(snap_1_group.id) + " vs. " + str(snap_2_group.id))
    #         if snap_1_group.id == snap_2_group.id:
    #             shared_groups.append({
    #                 'snap_1_group': snap_1_group,
    #                 'snap_2_group': snap_2_group
    #             })
    # print(shared_groups)
    #
    # oauth_client_profile = oauth_client.profile()
    #
    # return render_template("snapshots/compare_snapshots.html",
    #                        oauth_client_profile=oauth_client_profile, snapshot_1=snapshot_1, snapshot_2=snapshot_2,
    #                        shared_groups=shared_groups)


@app.route('/take_snapshot', methods=['POST', "GET"])
def take_snapshot():
    name = request.form['snapshot_name']
    groups_selected = request.form.getlist('groups_selected')
    print(groups_selected)
    if groups_selected:
        token = fetch_token(OAUTH_APP_NAME)
        print(token)
        gitlab_request = GitLabAPI(token["access_token"], GITLAB_HOST)
        if gitlab_request.is_valid_token():
            oauth_client_profile = oauth_client.profile()
            thread_id = start_snapshot(get_user_id(), name, groups_selected, token["access_token"])
            return render_template("snapshots/take_snapshot.html", hostname=GITLAB_HOST, token=token,
                                   oauth_client_profile=oauth_client_profile,
                                   thread_id=thread_id, snapshot_name=name)
        else:
            return render_template("utilities/invalid_token.html")
    else:
        return redirect(url_for('home', _external=True))


@app.route('/home')
def home():
    token = fetch_token(OAUTH_APP_NAME)
    gitlab_request = GitLabAPI(token["access_token"], GITLAB_HOST)
    if gitlab_request.is_valid_token():
        oauth_client_profile = oauth_client.profile()
        query = Owner.query.filter_by(username=oauth_client_profile.preferred_username).first()
        # You can handle rendering a "First time user" screen here
        if get_user_id() is None:
            api_key = secrets.token_urlsafe(20);
            token_set = fetch_token(OAUTH_APP_NAME)  # email is in here too
            print(token_set)
            owner = Owner(oauth_client_profile.preferred_username, oauth_client_profile.email,
                          token_set["access_token"], api_key)
            db.session.add(owner)
            db.session.commit()
        else:
            print("User already exists")
            print(query.id)

        snapshots = (Snapshot.query).filter_by(owner_id=get_user_id()).order_by(Snapshot.timestamp.desc()).all()

        # Solution to jinja2's inability to recognize an empty list as empty.
        is_snapshots = False
        for snapshot in snapshots:
            is_snapshots = True
            break

        # Link facts into their snapshots instance
        for snapshot in snapshots:
            print(snapshot.snapshot_id)
            snapshot.facts = (SnapshotFacts.query).filter_by(snapshot_id=snapshot.snapshot_id).one()

        parent_groups = gitlab_request.get_parent_groups()

        return render_template("index.html", hostname=GITLAB_HOST, token=token,
                               oauth_client_profile=oauth_client_profile,
                               thread_id=None, snapshots=snapshots,
                               parent_groups=parent_groups, is_snapshots=is_snapshots)
    else:
        return render_template("utilities/invalid_token.html")
