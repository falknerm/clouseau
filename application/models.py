from . import db, ma
from sqlalchemy import Integer, String, DateTime, ForeignKey
from sqlalchemy.sql import func


class Owner(db.Model):
    __tablename__ = 'owners'
    id = db.Column(Integer, primary_key=True)
    username = db.Column(String(100), unique=True)
    email = db.Column(String(100), unique=True)
    token = db.Column(String(500), unique=True)
    # https: // stackoverflow.com / questions / 39229186 / gitlab - refresh - oauth - token
    # After much research it turns out gitlab tokens are basically forever and the refresh functionality is dead code
    api_key = db.Column(String(500), unique=True)

    def __init__(self, username, email, token, api_key):
        self.username = username
        self.email = email
        self.token = token
        self.api_key = api_key

    def __repr__(self):
        return '<Owner %r>' % self.username


class Snapshot(db.Model):
    __tablename__ = 'snapshots'
    snapshot_id = db.Column(String(100), primary_key=True)
    timestamp = db.Column(DateTime(timezone=True), server_default=func.now())
    owner_id = db.Column(Integer, ForeignKey('owners.id'))
    name = db.Column(String(200))

    def __init__(self, owner_id, snapshot_id, name):
        self.owner_id = owner_id
        self.snapshot_id = snapshot_id
        self.name = name

    def __repr__(self):
        return '<Snapshot %r>' % self.snapshot_id


class SnapshotSchema(ma.SQLAlchemyAutoSchema):
    class Meta:
        model = Snapshot


class SnapshotFacts(db.Model):
    __tablename__ = 'snapshot_facts'
    snapshot_id = db.Column(String(100), ForeignKey('snapshots.snapshot_id'), primary_key=True)
    total_groups = db.Column(Integer)
    total_projects = db.Column(Integer)
    total_commits = db.Column(Integer)

    def __init__(self, snapshot_id, total_groups, total_projects, total_commits):
        self.snapshot_id = snapshot_id
        self.total_groups = total_groups
        self.total_projects = total_projects
        self.total_commits = total_commits

    def __repr__(self):
        return '<Snapshot Facts %r>' % self.snapshot_id


class Group(db.Model):
    __tablename__ = 'groups'
    snapshot_id = db.Column(String(100), ForeignKey('snapshots.snapshot_id'), primary_key=True)
    id = db.Column(Integer, primary_key=True)
    name = db.Column(String(100))
    description = db.Column(String(500))
    parent_group_id = db.Column(Integer)
    web_url = db.Column(String(500))
    visibility = db.Column(String(100))

    def __init__(self, snapshot_id, group_id, group_name=None, group_description=None,
                 parent_group_id=None, web_url=None, visibility=None):
        self.id = group_id
        self.name = group_name
        self.snapshot_id = snapshot_id
        self.description = group_description
        self.parent_group_id = parent_group_id
        self.web_url = web_url
        self.visibility = visibility

    def __repr__(self):
        return '<Group %r>' % self.id


class Project(db.Model):
    __tablename__ = 'projects'
    snapshot_id = db.Column(String(100), ForeignKey('snapshots.snapshot_id'), primary_key=True)
    group_id = db.Column(Integer, ForeignKey('groups.id'), primary_key=True)
    id = db.Column(Integer, primary_key=True)
    name = db.Column(String(100))
    description = db.Column(String(500))
    default_branch = db.Column(String(500))
    web_url = db.Column(String(500))
    avatar_url = db.Column(String(500))

    def __init__(self, snapshot_id, group_id, project_id, project_name=None,
                 project_description=None, default_branch=None, web_url=None, avatar_url=None):
        self.id = project_id
        self.group_id = group_id
        self.snapshot_id = snapshot_id
        self.name = project_name
        self.description = project_description
        self.default_branch = default_branch
        self.web_url = web_url
        self.avatar_url = avatar_url

    def __repr__(self):
        return '<Project %r>' % self.id


class Commit(db.Model):
    __tablename__ = 'commits'
    project_id = db.Column(Integer, ForeignKey('projects.id'), primary_key=True)
    commit_hash = db.Column(String(150), primary_key=True)
    short_hash = db.Column(String(150))
    created_timestamp = db.Column(DateTime(timezone=True))
    title = db.Column(String(300))
    message = db.Column(String(500))
    author_name = db.Column(String(100))
    author_email = db.Column(String(100))
    committer_name = db.Column(String(500))
    committer_email = db.Column(String(500))
    committed_timestamp = db.Column(DateTime(timezone=True))
    additions = db.Column(Integer)
    deletions = db.Column(Integer)
    total = db.Column(Integer)
    web_url = db.Column(String(300), unique=True)

    def __init__(self, project_id, commit_hash, short_hash, created_timestamp, title, message, author_name,
                 author_email,
                 committer_name, committer_email, committed_timestamp, additions, deletions, total, web_url):
        self.project_id = project_id
        self.commit_hash = commit_hash
        self.short_hash = short_hash
        self.created_timestamp = created_timestamp
        self.title = title
        self.message = message
        self.author_name = author_name
        self.author_email = author_email

        self.committer_name = committer_name
        self.committer_email = committer_email
        self.committed_timestamp = committed_timestamp
        self.additions = additions
        self.deletions = deletions
        self.total = total
        self.web_url = web_url

    def __repr__(self):
        return '<Commit %r>' % self.commit_hash


class Member(db.Model):
    __tablename__ = 'members'
    id = db.Column(Integer, primary_key=True)
    email = db.Column(String(500))
    username = db.Column(String(500))
    avatar_url = db.Column(String(500))
    name = db.Column(String(500))
    web_url = db.Column(String(500))
    state = db.Column(String(80))
    access_level = db.Column(Integer)
    colour = db.Column(String(500))

    def __init__(self, id, email, username, avatar_url, name, web_url, state, access_level, colour):
        self.id = id
        self.email = email
        self.username = username
        self.avatar_url = avatar_url
        self.name = name
        self.web_url = web_url
        self.state = state
        self.access_level = access_level
        self.colour = colour

    def __repr__(self):
        return '<Member %r>' % self.username


class MemberEmailRelation(db.Model):
    owner_id = db.Column(Integer, ForeignKey('owners.id'), primary_key=True)
    member_id = db.Column(Integer, ForeignKey('members.id'), primary_key=True)
    email = db.Column(String(500), primary_key=True)

    def __init__(self, member_id, email, owner_id):
        self.member_id = member_id
        self.email = email
        self.owner_id = owner_id

    def __repr__(self):
        return '<MemberEmailRelation %r>' % self.email


class UnclaimedEmailCommits(db.Model):
    id = db.Column(String(100), primary_key=True)
    snapshot_id = db.Column(String(100), ForeignKey('snapshots.snapshot_id'), primary_key=True)
    group_id = db.Column(Integer, ForeignKey('groups.id'), primary_key=True)
    project_id = db.Column(Integer, ForeignKey('projects.id'), primary_key=True)
    email = db.Column(String(500), primary_key=True)
    commits = db.Column(Integer)
    lines_added = db.Column(Integer)
    lines_removed = db.Column(Integer)
    total_lines_changed = db.Column(Integer)

    def __init__(self, id, snapshot_id, group_id, project_id, email, commits=0,
                 lines_added=0,
                 lines_removed=0,
                 total_lines_changed=0):
        self.id = id
        self.snapshot_id = snapshot_id
        self.group_id = group_id
        self.project_id = project_id
        self.email = email
        self.commits = commits
        self.lines_added = lines_added
        self.lines_removed = lines_removed
        self.total_lines_changed = total_lines_changed

    def __repr__(self):
        return '<UnclaimedEmailCommits %r>' % self.email


class MemberGroupRelation(db.Model):
    snapshot_id = db.Column(String(100), ForeignKey('snapshots.snapshot_id'), primary_key=True)
    group_id = db.Column(Integer, ForeignKey('groups.id'), primary_key=True)
    member_id = db.Column(Integer, ForeignKey('members.id'), primary_key=True)

    def __init__(self, snapshot_id, group_id, member_id):
        self.snapshot_id = snapshot_id
        self.group_id = group_id
        self.member_id = member_id

    def __repr__(self):
        return '<MemberGroupRelation %r>' % self.member_id


class MemberProjectRelation(db.Model):
    snapshot_id = db.Column(String(100), ForeignKey('snapshots.snapshot_id'), primary_key=True)
    project_id = db.Column(Integer, ForeignKey('projects.id'), primary_key=True)
    member_id = db.Column(Integer, ForeignKey('members.id'), primary_key=True)

    def __init__(self, snapshot_id, project_id, member_id):
        self.snapshot_id = snapshot_id
        self.project_id = project_id
        self.member_id = member_id

    def __repr__(self):
        return '<MemberProjectRelation %r>' % self.member_id


class UserFactTable(db.Model):
    snapshot_id = db.Column(String(100), ForeignKey('snapshots.snapshot_id'), primary_key=True)
    project_id = db.Column(Integer, ForeignKey('projects.id'), primary_key=True)
    member_id = db.Column(Integer, ForeignKey('members.id'), primary_key=True)
    commits = db.Column(Integer)
    lines_added = db.Column(Integer)
    lines_removed = db.Column(Integer)
    total_lines_changed = db.Column(Integer)
    merge_requests_participated = db.Column(Integer)
    merge_requests_author = db.Column(Integer)
    merge_requests_assignee = db.Column(Integer)

    def __init__(self, snapshot_id, project_id, member_id, commits=0, lines_added=0,
                 lines_removed=0,
                 total_lines_changed=0,
                 merge_requests_participated=0,
                 merge_requests_author=0,
                 merge_requests_assignee=0,
                 ):
        self.snapshot_id = snapshot_id
        self.project_id = project_id
        self.member_id = member_id
        self.commits = commits
        self.lines_added = lines_added
        self.lines_removed = lines_removed
        self.total_lines_changed = total_lines_changed
        self.merge_requests_participated = merge_requests_participated
        self.merge_requests_author = merge_requests_author
        self.merge_requests_assignee = merge_requests_assignee

    def __repr__(self):
        return '<UserFactTable %r>' % self.member_id
