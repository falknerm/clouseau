import threading
import uuid
from application.support.GitlabAPI import GitLabAPI
from ..models import db, Owner, Snapshot, SnapshotFacts, Group, Project, Commit, Member, MemberGroupRelation, MemberProjectRelation, \
    UserFactTable, UnclaimedEmailCommits, MemberEmailRelation
from .. import create_app
from sqlalchemy import exc, select, exists
from datetime import datetime
from collections import defaultdict
from copy import copy
import collections
import random

def makehash():
    return collections.defaultdict(makehash)


# Snapshotter
#
# This class is instantiated on a new thread and stored in main flask loop thread pool
#
# The entry point is the method "run"
#
class Snapshotter(threading.Thread):
    def __init__(self, gitlab_host, token, owner_id, snapshot_name, groups_selected):
        self.gitlab_request = GitLabAPI(token, gitlab_host)
        self.owner_id = owner_id
        self.token = token
        self.group_progress = 0
        self.current_group = ""
        self.project_progress = 0
        self.current_project = ""
        self.gitlab_host = gitlab_host
        self.groups_selected = groups_selected
        self.snapshot_name = snapshot_name
        self.snapshot_id = str(uuid.uuid4())
        super().__init__()

    def sift_merge_request_data(self, all_project_merge_requests):
        sifted_data = makehash()
        '''
            Structure: 
            {
                'member_id' : {     
                        num_participated_mr : 0,
                        num_created_mr : 0,
                }
            
            }
        '''
        #Track participantion across all merge requests for each user found, does not track users who don't MR
        for merge_request in all_project_merge_requests:
            for participant in merge_request['participants']:
                if sifted_data[participant["id"]]['participated']:
                    sifted_data[participant["id"]]['participated'] += 1
                else:
                    sifted_data[participant["id"]]['participated'] = 1
            if merge_request['author']:
                if sifted_data[merge_request['author']['id']]['author']:
                    sifted_data[merge_request['author']['id']]['author'] += 1
                else:
                    sifted_data[merge_request['author']['id']]['author'] = 1
            if merge_request['assignee']:
                if sifted_data[merge_request['assignee']['id']]['assignee']:
                    sifted_data[merge_request['assignee']['id']]['assignee'] += 1
                else:
                    sifted_data[merge_request['assignee']['id']]['assignee'] = 1

        return sifted_data




    # create_member_email_relation
    # params:
    #   - member_id: (int) ID of the member you want to relate this email address to
    #   - email: (string) the email address you want to relate to the member
    #
    # purpose:
    #   - reduce duplicate code, save relation to the database
    def create_member_email_relation(self, member_id, email):
        member_group_relation = MemberEmailRelation(member_id, email, self.owner_id)
        db.session.add(member_group_relation)
        db.session.commit()

    # create_member_sqlalch
    # params:
    #   - member: json Object from GitLab API
    #
    # purpose:
    #   - create a new Member Database object, return to caller, save relation of new member to email in database
    def create_member_sqlalch(self, member):
        id = member['id']
        username = member['username']

        # NOTE: Because of privilege limitation in the gitlab api
        #       (User access to members vs. Admin access to members)
        #       we are unable to get a members email addresses
        #       as a result, I currently guess their @uoguelph email
        #       but this should work, because username is start of email
        #       example: falknerm -> falknerm@uoguelph.ca
        if 'email' in member:
            email = member['email']
        else:
            email = username + '@uoguelph.ca'

        self.create_member_email_relation(id, email)
        r = lambda: random.randint(0, 255)
        colour = '#{:02x}{:02x}{:02x}'.format(r(), r(), r())
        avatar_url = member['avatar_url']
        name = member['name']
        web_url = member['web_url']
        state = member['state']
        access_level = member['access_level']
        member_sql = Member(id, email, username, avatar_url, name, web_url, state, access_level, colour)

        return member_sql

    # get_member_from_email
    # params:
    #   - email_address (string)
    # return:
    #   - Member with that email address, or None if it doesn't exist
    def get_member_from_email(self, email_address):
        memberEmailRelation = (MemberEmailRelation.query).filter_by(email=email_address).first()
        if memberEmailRelation is None:
            return None
        else:
            return Member.query.filter_by(id=memberEmailRelation.member_id).first()

    # create_commit_sqlalch
    # params:
    #   - commit (json from GitLabAPI)
    #   - project_id (integer)
    # return:
    #   - Commit SqlAlchemy Object
    # purpose:
    #   - create a SQLAlchemy Commit Object to be committed to the database with correct timestamps
    def create_commit_sqlalch(self, commit, project_id):
        commit_hash = commit['id']
        short_hash = commit['short_id']
        created_timestamp = datetime.strptime(commit['created_at'], '%Y-%m-%dT%H:%M:%S.%f%z')
        title = commit['title']
        message = commit['message']
        author_name = commit['author_name']
        author_email = commit['author_email']
        committer_name = commit['committer_name']
        committer_email = commit['committer_email']
        committed_timestamp = datetime.strptime(commit['committed_date'], '%Y-%m-%dT%H:%M:%S.%f%z')
        additions = commit['stats']['additions']
        deletions = commit['stats']['deletions']
        total = commit['stats']['total']
        web_url = commit['web_url']
        commit_sql = Commit(project_id, commit_hash, short_hash,
                            created_timestamp, title, message, author_name, author_email,
                            committer_name, committer_email, committed_timestamp,
                            additions, deletions, total, web_url)
        return commit_sql

    # run()
    # purpose:
    #   - take the snapshot on a new thread.
    #   - put everything in the database
    #   - have everything in the database correctly linked correctly
    #   - compile meta data/ stats for plotting, such that it doesn't need to be computed on page load
    #   - keep track of the current status of the thread for loading times in self
    # notes:
    #   - Yes, its a monolith! But to keep full fidelity of relationships, and keep it easy to read, it had to be done.
    #
    #    [Relationships map to the structure (loops) of this method]
    #    - Snapshot
    #       - groups (both subgroups and parent groups)
    #           - the group members
    #           - projects
    #               - project members
    #               - commits
    #               - compiled statistics
    #
    #     In this structure, each child needs its parent, and grandparents ids, etc. They are tightly coupled.
    #
    #     You **could** separate run() into 3 dozen smaller functions that each take 6 parameters. However, it would
    #     be completely impossible to understand the flow for new eyes on it.
    #     I designed it this way for both of our sanity.
    #
    def run(self):
        # allows us to have the context of the app while on a separate thread
        app = create_app()
        app.app_context().push()

        #Counters for Snapshot facts
        total_groups = 0
        total_projects = 0
        total_commits = 0


        # Insert this snapshots details into the database
        snapshot = Snapshot(self.owner_id, self.snapshot_id, self.snapshot_name)
        db.session.add(snapshot)
        db.session.commit()

        # Before we try anything, check to see if the token is valid
        if self.gitlab_request.is_valid_token():

            # Combine subgroups and parent groups for commit to database
            # subgroups can still be identified as subgroups with  `if group['parent_id']:`
            groups = self.gitlab_request.get_groups_with_ids(self.groups_selected)
            subgroups = self.gitlab_request.get_subgroups_with_parent_ids(self.groups_selected)
            groups.extend(subgroups)

            # User has no groups, end the snapshot
            if len(groups) == 0:
                self.group_progress = 100
                return
            # given n number of groups, figure out, out of 100%, each would take - when completed
            # used in the loading screen
            progress_per_group = 100 / len(groups)

            #track the total number of groups
            total_groups = total_groups + len(groups)

            for group in groups:

                self.current_group = group['name']

                # if group does not have a parent, its parent ID in the database will be NULL
                parent_id = None
                if group['parent_id']:
                    parent_id = group['parent_id']

                # save the group to the database
                group_sql = Group(self.snapshot_id,
                                  group['id'], group['name'],
                                  group['description'], parent_id,
                                  group['web_url'], group['visibility'])
                db.session.add(group_sql)
                db.session.commit()

                # for each group, download their members
                group_members = self.gitlab_request.get_group_members(group['id'])
                for group_member in group_members:

                    # save that group member, and its relation to this snapshot, and group to the database
                    member_group_relation = MemberGroupRelation(self.snapshot_id, group['id'], group_member['id'])
                    db.session.add(member_group_relation)
                    db.session.commit()

                    # If this member already exists, do not add them to the global member database
                    # If the member has never been encountered before, add them to the global member database
                    # Either way the relation above has to point to something
                    if Member.query.filter_by(id=group_member['id']).first() is None:
                        sql_member = self.create_member_sqlalch(group_member)
                        db.session.add(sql_member)
                        db.session.commit()
                        print("Added new member" + group_member['username'])

                # Get all of the projects in the current group
                projects = self.gitlab_request.get_projects(group['id'])

                # If the group has no projects, continue with the next group.
                if len(projects) == 0:
                    self.project_progress = 100
                    self.group_progress += progress_per_group
                    self.current_project = "Group has no repos"
                    continue

                # given n number of projects, figure out, out of 100%, each would take - when completed
                # used in the loading screen, set to 0 in case this is not the first project
                progress_per_project = 100 / len(projects)
                self.project_progress = 0

                # Keeping track of the total number of projects held in this snapshot
                total_projects = total_projects + len(projects)

                for project in projects:
                    self.current_project = project['name']

                    # save the current project to the database
                    project_id = project['id']
                    default_branch = project['default_branch']

                    project_sql = Project(self.snapshot_id, group['id'], project['id'], project['name'],
                                          project['description'], default_branch, project['web_url'],
                                          project['avatar_url']
                                          )
                    db.session.add(project_sql)
                    db.session.commit()

                    # Get the current project's members
                    project_members = self.gitlab_request.get_project_members(project['id'])
                    for project_member in project_members:

                        # create relation between a project and a member and save it to the database
                        project_member_relation = MemberProjectRelation(self.snapshot_id, project['id'],
                                                                        project_member['id'])
                        db.session.add(project_member_relation)
                        db.session.commit()

                        # if the current member has not already been added to the database, insert them
                        if Member.query.filter_by(id=project_member['id']).first() is None:
                            sql_member = self.create_member_sqlalch(project_member)
                            db.session.add(sql_member)
                            db.session.commit()
                            print("Added new member" + project_member['username'])

                    print(project_id)

                    #get all projects merge requests (pagination automatic)
                    all_project_merge_requests = self.gitlab_request.get_all_merge_requests(project_id)
                    for merge_request in all_project_merge_requests:
                        #Link all participant info into merge_request data
                        merge_request["participants"] = self.gitlab_request.\
                            get_merge_request_participants(project_id, merge_request['iid'])

                    #Reverses the relationship from Merge Requests -> User
                    #to User -> Merge requests
                    sifted_merge_request_data = self.sift_merge_request_data(all_project_merge_requests)

                    # for member_id in sifted_merge_request_data: #is member Id actually member ID
                    #     if UserFactTable.query.filter_by(snapshot_id member_id=member_id).first() is None:
                    for member_id in sifted_merge_request_data:

                        # if the values of sifted_merge_request, such as: sifted_merge_request_data[member_id]['participated'] are not
                        # present, the make_hash() function will just return a new dict, which is not okay for a DB
                        # insertion. As a result, I need to check to see if the values exist before putting into the database
                        temp_participated = 0
                        temp_author = 0
                        temp_assignee = 0

                        if sifted_merge_request_data[member_id]['participated']:
                            temp_participated = sifted_merge_request_data[member_id]['participated']
                        if sifted_merge_request_data[member_id]['author']:
                            temp_author = sifted_merge_request_data[member_id]['author']
                        if sifted_merge_request_data[member_id]['assignee']:
                            temp_assignee = sifted_merge_request_data[member_id]['assignee']

                        sql_fact = UserFactTable(snapshot_id=self.snapshot_id, project_id=project_id,
                                                 member_id=member_id,
                                                 merge_requests_participated=temp_participated,
                                                 merge_requests_author=temp_author,
                                                 merge_requests_assignee=temp_assignee,
                                                 )
                        db.session.add(sql_fact)
                        db.session.commit()




                    # get all the commits for this project. This manages pagination too.
                    commits = self.gitlab_request.get_all_commits_in_list(project_id)

                    # pre make the collections to avoid bad cases
                    existing_members_commits = makehash()
                    unclaimed_members_commits = makehash()

                    total_commits = total_commits + len(commits)
                    for commit in commits:
                        # create the SqlAlchemy object for a commit
                        commit_sql = self.create_commit_sqlalch(commit, project_id)
                        # find, if they exist, the member that is associated with this commit
                        commit_member = self.get_member_from_email(commit_sql.committer_email)

                        # If this no member is found, it becomes UNCLAIMED, and all commits with this unclaimed
                        # member must be tabulated accordingly
                        if commit_member is None:
                            email = commit_sql.committer_email
                            if unclaimed_members_commits[email]['commits']:
                                unclaimed_members_commits[email]['commits'] += 1
                            else:
                                unclaimed_members_commits[email]['commits'] = 1

                            if unclaimed_members_commits[email]['additions']:
                                unclaimed_members_commits[email]['additions'] += commit_sql.additions
                            else:
                                unclaimed_members_commits[email]['additions'] = commit_sql.additions

                            if unclaimed_members_commits[email]['deletions']:
                                unclaimed_members_commits[email]['deletions'] += commit_sql.deletions
                            else:
                                unclaimed_members_commits[email]['deletions'] = commit_sql.deletions

                            if unclaimed_members_commits[email]['total']:
                                unclaimed_members_commits[email]['total'] += commit_sql.total
                            else:
                                unclaimed_members_commits[email]['total'] = commit_sql.total
                        # Member found, tabulate all their contributions
                        else:
                            if existing_members_commits[commit_member.id]['commits']:
                                existing_members_commits[commit_member.id]['commits'] += 1
                            else:
                                existing_members_commits[commit_member.id]['commits'] = 1

                            if existing_members_commits[commit_member.id]['additions']:
                                existing_members_commits[commit_member.id]['additions'] += commit_sql.additions
                            else:
                                existing_members_commits[commit_member.id]['additions'] = commit_sql.additions

                            if existing_members_commits[commit_member.id]['deletions']:
                                existing_members_commits[commit_member.id]['deletions'] += commit_sql.deletions
                            else:
                                existing_members_commits[commit_member.id]['deletions'] = commit_sql.deletions

                            if existing_members_commits[commit_member.id]['total']:
                                existing_members_commits[commit_member.id]['total'] += commit_sql.total
                            else:
                                existing_members_commits[commit_member.id]['total'] = commit_sql.total
                        # try to add the current commit to the database
                        # however, the database only allows one instance of a given commit to be saved
                        # if the commit has already been saved in another snapshot, do not save it again
                        try:
                            db.session.add(commit_sql)
                            db.session.commit()
                        except exc.IntegrityError:
                            db.session.rollback()
                            continue

                    # Iterate all the tabulated data for existing members, and save it to the user fact table
                    for member_commits_id in existing_members_commits:

                        #Check to see if facts for this user already exist
                        #If the facts already exist, update the existing entrty in the database
                        #Else Create a new entry
                        existing_facts = UserFactTable.query.filter_by(member_id=member_commits_id, snapshot_id=self.snapshot_id, project_id=project['id']).first()
                        if existing_facts is not None:
                            existing_facts.commits = existing_members_commits[member_commits_id]['commits']
                            existing_facts.lines_added = existing_members_commits[member_commits_id]['additions']
                            existing_facts.lines_removed = existing_members_commits[member_commits_id]['deletions']
                            existing_facts.total_lines_changed = existing_members_commits[member_commits_id]['total']
                            db.session.commit()

                        else:
                            user_fact = UserFactTable(self.snapshot_id, project['id'],
                                                      member_commits_id,
                                                      existing_members_commits[member_commits_id]['commits'],
                                                      existing_members_commits[member_commits_id]['additions'],
                                                      existing_members_commits[member_commits_id]['deletions'],
                                                      existing_members_commits[member_commits_id]['total']
                                                      )

                            db.session.add(user_fact)
                            db.session.commit()

                    # Iterate over all the tabulated data for UNCLAIMED users, and save it to the unclaimed table
                    for unclaimed_email in unclaimed_members_commits:
                        # Creates a Unique uuid for the unclaimed data, so it can be easily referenced during
                        # the claiming process.
                        claim_id = str(uuid.uuid4())
                        unclaimed_email_sql = UnclaimedEmailCommits(id=claim_id,
                                                                    snapshot_id=self.snapshot_id, group_id=group['id'], project_id=project_id,
                                                                    email=unclaimed_email,
                                                                    commits=unclaimed_members_commits[unclaimed_email][
                                                                        'commits'],
                                                                    lines_added=unclaimed_members_commits[unclaimed_email][
                                                                        'additions'],
                                                                    lines_removed=unclaimed_members_commits[unclaimed_email][
                                                                        'deletions'],
                                                                    total_lines_changed=unclaimed_members_commits[unclaimed_email][
                                                                        'total']
                                                                    )
                        db.session.add(unclaimed_email_sql)
                        db.session.commit()

                    # Project complete: Save progress increase to overall.
                    self.project_progress += progress_per_project

                # Group complete: there are more groups coming, save the progress to increase overall.
                if len(groups) != 1:
                    self.group_progress += progress_per_group

            #Quickly save the overall stats of the snapshot to the DB
            snapshot_facts = SnapshotFacts(self.snapshot_id, total_groups, total_projects, total_commits)
            db.session.add(snapshot_facts)
            db.session.commit()

            #Done!
            self.group_progress = 100
            self.project_progress = 100
