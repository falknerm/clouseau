import json

import requests


class GitLabAPI(object):
    def __init__(self, oauth_token, hostname):
        self.oauth_token = oauth_token
        self.http_bearer = "Bearer " + self.oauth_token
        self.hostname = hostname
        self.base_url = 'https://{hostname}/api/v4/'.format(hostname=hostname)

    def is_valid_token(self):
        headers = {'Authorization': self.http_bearer}
        r = requests.get('https://{hostname}/oauth/token/info'.format(hostname=self.hostname), headers=headers)

        if r.status_code == 200:
            return True
        else:
            print(r.status_code)
            print(r.text)
            return False

    def get_token_scope(self):
        headers = {'Authorization': self.http_bearer}
        r = requests.get('https://{hostname}/oauth/token/info'.format(hostname=self.hostname), headers=headers)
        if r.status_code == 200:
            return r.text
        else:
            raise Exception('groups_request returned status code' + r.status_code)

    def get(self, endpoint):
        headers = {
            'Authorization': self.http_bearer,
            'Content-Type': 'application/json'
        }
        return requests.get(self.base_url + endpoint, headers=headers)

    def get_user(self, username):
        headers = {'Authorization': self.http_bearer}
        params = {'username': username}
        return requests.get(self.base_url + 'users', headers=headers, params=params)

    def get_parent_groups(self):
        headers = {'Authorization': self.http_bearer,
                   'Content-Type': 'application/json'}
        groups_request = requests.get(self.base_url + 'groups', headers=headers)

        print(self.oauth_token)
        if groups_request.status_code == 200:
            groups_raw_text = groups_request.text
            groups = json.loads(groups_raw_text)


            parent_groups = [
                group
                for group in groups
                if group['parent_id'] is None
            ]

            print(parent_groups)
            return parent_groups
        else:
            raise Exception('groups_request returned status code' + groups_request.status_code)

    #Given list of Group IDS ex: [1,2,3,4] return API data
    def get_groups_with_ids(self, groups_selected):
        groups = []
        for group_id in groups_selected:
            group = self.get_group(group_id)
            groups.append(group)

        return groups


    def get_subgroups(self, parent_id):
        headers = {'Authorization': self.http_bearer}
        subgroup_request = requests.get(
            self.base_url + 'groups/{parent_id}/subgroups'.format(parent_id=parent_id), headers=headers)
        if subgroup_request.status_code == 200:
            subgroup_raw_text = subgroup_request.text
            subgroups = json.loads(subgroup_raw_text)
            return subgroups
        else:
            raise Exception('group_members_request returned status code' + str(subgroup_request.status_code))

    #Given list of Groups IDS ex [1,2,3] of which you want their subgroups
    def get_subgroups_with_parent_ids(self, parent_ids):
        all_subgroups = []
        for parent_id in parent_ids:
            subgroups = self.get_subgroups(parent_id)
            for subgroup in subgroups:
                all_subgroups.append(subgroup)

        return all_subgroups

    def get_user_with_id(self, user_id): 
        headers = {'Authorization': self.http_bearer}
        user_request = requests.get(
            self.base_url + 'user/emails/{user_id}'.format(user_id=user_id), headers=headers)
        if user_request.status_code == 200:
            user_raw_text = user_request.text
            user = json.loads(user_raw_text)
            return user
        else:
            raise Exception('group_members_request returned status code' + str(user_request.status_code))

    def get_group_members(self, group_id): 
        headers = {'Authorization': self.http_bearer}
        members_request = requests.get(
            self.base_url + 'groups/{group_id}/members'.format(group_id=group_id), headers=headers)
        if members_request.status_code == 200:
            members_raw_text = members_request.text
            members = json.loads(members_raw_text)
            return members
        else:
            raise Exception('group_members_request returned status code' + members_request.status_code)

    def get_group(self, group_id):
        headers = {'Authorization': self.http_bearer}
        group_request = requests.get(
            self.base_url + 'groups/{group_id}'.format(group_id=group_id), headers=headers)
        if group_request.status_code == 200:
            group_raw_text = group_request.text
            group = json.loads(group_raw_text)
            return group
        else:
            raise Exception('group_members_request returned status code' + group_request.status_code)


    def get_project_members(self, project_id): 
        headers = {'Authorization': self.http_bearer}
        members_request = requests.get(
            self.base_url + 'projects/{project_id}/members/all'.format(project_id=project_id), headers=headers)
        if members_request.status_code == 200:
            members_raw_text = members_request.text
            members = json.loads(members_raw_text)
            return members
        else:
            raise Exception('project_members_request returned status code' + members_request.status_code)


    #Merge requests CREATED by user
    def get_merge_requests_by_user(self, user_id, project_id):
        headers = {'Authorization': self.http_bearer}
        params = {'author_id': user_id}
        mr_request = requests.get(
            self.base_url + 'projects/{project_id}/merge_requests'.format(project_id=project_id),
            headers=headers, params=params)
        if mr_request.status_code == 200:
            mr_request_raw_text = mr_request.text
            mrs = json.loads(mr_request_raw_text)
            return mrs
        else:
            raise Exception('projects_request returned status code' + mr_request.status_code)


    #https://stackoverflow.com/questions/38399115/gitlab-api-accept-merge-request-fails
    def get_merge_request_participants(self, project_id, merge_request_id):
        headers = {'Authorization': self.http_bearer}
        mr_request = requests.get(
            self.base_url + 'projects/{project_id}/merge_requests/{merge_request_id}/participants'.format(project_id=project_id, merge_request_id=merge_request_id),
            headers=headers)
        if mr_request.status_code == 200:
            mr_request_raw_text = mr_request.text
            mrs = json.loads(mr_request_raw_text)
            return mrs
        else:
            raise Exception('projects_request returned status code' + mr_request.status_code)


    def get_projects(self, group_id):
        headers = {'Authorization': self.http_bearer}
        projects_request = requests.get(
            self.base_url + 'groups/{group_id}/projects'.format(group_id=group_id), headers=headers)
        if projects_request.status_code == 200:
            projects_raw_text = projects_request.text
            projects = json.loads(projects_raw_text)
            return projects
        else:
            raise Exception('projects_request returned status code' + projects_request.status_code)

    # All Merge requests for a project
    def get_all_merge_requests(self, project_id):
        headers = {'Authorization': self.http_bearer}
        params = {'scope': 'all'}
        mr_request = requests.get(
            self.base_url + 'projects/{project_id}/merge_requests'.format(project_id=project_id),
            headers=headers, params=params)
        response_headers = mr_request.headers
        if mr_request.status_code == 200:
            merge_requests_raw_text = mr_request.text
            merge_requests = json.loads(merge_requests_raw_text)
            print(
                        "This repo has " + mr_request.headers['X-Total-Pages'] +
                        " commits with " + mr_request.headers['X-Per-Page'] +
                        " commits per page"
                    )
            if mr_request.headers['X-Page'] == mr_request.headers['X-Total-Pages']:
                print(
                    "Finished: " + mr_request.headers['X-Page'] + "/ " + mr_request.headers['X-Total-Pages']
                    + " - DONE!"
                )
                return merge_requests

            '''
            Handling Pagination of GitLab Requests
            '''
            while mr_request.headers['X-Page'] < mr_request.headers['X-Total-Pages']:
                print(
                    "Finished: " + mr_request.headers['X-Page'] + "/ " + mr_request.headers['X-Total-Pages']
                         + "   -  Starting " + mr_request.headers['X-Next-Page'] + "/ "
                    + mr_request.headers['X-Total-Pages']
                )
                outbound_headers = {'Authorization': self.http_bearer}
                params = {
                          'page': mr_request.headers['X-Next-Page']
                          }
                #print(params)
                mr_request = requests.get(
                    self.base_url + 'projects/{project_id}/merge_requests'.format(project_id=project_id),
                    headers=outbound_headers, params=params)
                mr_raw_text = mr_request.text
                new_mrs = json.loads(mr_raw_text)
                for new_merge_request in new_mrs:
                    merge_requests.append(new_merge_request)

            return merge_requests

        else:
            raise Exception('projects_request returned status code' + mr_request.status_code)

    # DONE: Alternate solution that works given weird gitlab bug with X-Total pages
    def get_all_commits_in_list(self, project_id):
        per_page = '50'
        headers = {'Authorization': self.http_bearer}
        params = {'with_stats': 'true',
                  'per_page': per_page
                  }
        commits_request = requests.get(
            self.base_url + 'projects/{project_id}/repository/commits'.format(project_id=project_id),
            headers=headers, params=params)

        headers = commits_request.headers

        if commits_request.status_code == 200:
            commits_raw_text = commits_request.text
            commits = json.loads(commits_raw_text)

            if commits_request.headers['X-Next-Page'] == '':
                print(
                    "Found 1 page of commits. Done!"
                )
                return commits

            # While there is a next page, get the next page
            while commits_request.headers['X-Next-Page'] != '':

                print(
                    "On Page " + commits_request.headers['X-Page'] + " with the next page being "
                    + commits_request.headers['X-Next-Page']
                )

                outbound_headers = {'Authorization': self.http_bearer}
                next_page = commits_request.headers['X-Next-Page']
                outbound_params = {'with_stats': 'true',
                                   'per_page': per_page,
                                   'page': commits_request.headers['X-Next-Page']
                                   }
                commits_request = requests.get(
                    self.base_url + 'projects/{project_id}/repository/commits'.format(project_id=project_id),
                    headers=outbound_headers, params=outbound_params)

                commits_raw_text = commits_request.text
                new_commits = json.loads(commits_raw_text)
                for new_commit in new_commits:
                    commits.append(new_commit)
            print("---Done!")
            return commits
        else:
            raise Exception('commits_request returned status code' + commits_request.status_code)




    # def get_list_of_commits(self, project_id):
    #     headers = {'Authorization': self.http_bearer}
    #     params = {'with_stats': 'true',
    #               'per_page': '100'
    #               }
    #     commits_request = requests.get(
    #         self.base_url + 'projects/{project_id}/repository/commits'.format(project_id=project_id),
    #         headers=headers, params=params)
    #
    #     headers = commits_request.headers
    #
    #     if commits_request.status_code == 200:
    #         commits_raw_text = commits_request.text
    #         commits = json.loads(commits_raw_text)
    #
    #         print(
    #             "This repo has " + headers['X-Total-Pages'] +
    #             " commits with " + headers['X-Per-Page'] +
    #             " commits per page"
    #         )
    #
    #
    #         if commits_request.headers['X-Page'] == commits_request.headers['X-Total-Pages']:
    #             print(
    #                 "Finished: " + commits_request.headers['X-Page'] + "/ " + commits_request.headers['X-Total-Pages']
    #                 + " - DONE!"
    #             )
    #             return commits
    #
    #         '''
    #         Handling Pagination of GitLab Requests
    #         '''
    #         while commits_request.headers['X-Page'] < commits_request.headers['X-Total-Pages']:
    #             print(
    #                 "Finished: " + commits_request.headers['X-Page'] + "/ " + commits_request.headers['X-Total-Pages']
    #                      + "   -  Starting " + commits_request.headers['X-Next-Page'] + "/ "
    #                 + commits_request.headers['X-Total-Pages']
    #             )
    #             outbound_headers = {'Authorization': self.http_bearer}
    #             params = {'with_stats': 'true',
    #                       'per_page': '100',
    #                       'page': commits_request.headers['X-Next-Page']
    #                       }
    #             #print(params)
    #             commits_request = requests.get(
    #                 self.base_url + 'projects/{project_id}/repository/commits'.format(project_id=project_id),
    #                 headers=outbound_headers, params=params)
    #             commits_raw_text = commits_request.text
    #             new_commits = json.loads(commits_raw_text)
    #             for new_commit in new_commits:
    #                 commits.append(new_commit)
    #
    #         return commits
    #
    #     else:
    #         raise Exception('commits_request returned status code' + commits_request.status_code)
